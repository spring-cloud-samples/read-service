package com.hendisantika.springcloud.readservice;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * Created by IntelliJ IDEA.
 * Project : read-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-06
 * Time: 06:59
 * To change this template use File | Settings | File Templates.
 */
@Service
public class BookService {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    @Lazy
    private EurekaClient discoveryClient;

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @HystrixCommand(fallbackMethod = "reliable")
    public String readingList() {
        return this.restTemplate.getForObject(URI.create(serviceUrl() + "recommended"), String.class);
    }

    public String serviceUrl() {
        InstanceInfo instance = discoveryClient.getNextServerFromEureka("RECOMMENDED", false);
        return instance.getHomePageUrl();
    }

    public String reliable() {
        return "Cloud Native Java (O'Reilly)";
    }

    @HystrixCommand(fallbackMethod = "reliableBuy")
    public String buy() {
        return this.restTemplate.getForObject(URI.create(serviceUrl() + "buy"), String.class);
    }

    public String reliableBuy() {
        return "Any Java 8 Book";
    }
}
